
# Модуль - это любой программный файл на python, т.е. любой файл с расширением *.py

print(f"Start -> {__name__}")

# (1)
#import labModuleUtils

#if __name__ == "__main__":
#    labModuleUtils.show_numders(10)
#    labModuleUtils.show_words("азментесвайн", 4)

# (2)
from labModuleUtils import show_numders

if __name__ == "__main__":
    show_numders(8)

print(f"End -> {__name__}")
# sdmgs