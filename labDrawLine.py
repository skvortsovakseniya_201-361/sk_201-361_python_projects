# Нарисовать в консоли линию с параметрами: ширина

char = '*' #символ рисования

print("Длина линии?")
w = int(input())

# (1)
print(char * w)

# (2)
for i in range(w):
    print(char, end='')
    # ksnfv