# labColor16
# Вывести в консоль все доступные цвета и фоны 16
# \033 - обозначение что дальше идет цвет [2 - толщина шрифта ;{num}m - цвет текста {num} - цифра \033[0;0m - цвет фона

def colors_16(color_):
    return("\033[1;{num}m {num} \033[0;0m".format(num = str(color_)))

print("Доступные цвета:")
print(''.join([colors_16(x) for x in range (30,38)]))

print("Доступные фоны:")
print(''.join([colors_16(x) for x in range (40,48)]))
    # kdfnbsf