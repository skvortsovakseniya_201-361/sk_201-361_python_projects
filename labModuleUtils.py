print(f"Start -> {__name__}")

def show_numders(n:int):
    print(*range(n))

def show_words(word: str, n: int):
    print(*[f"{word} {i}" for i in range(n)], sep='\n')

if __name__ == "__main__":
    show_numders(5)
    show_words("адептещау", 5)

print(f"End -> {__name__}")
# dkgn
