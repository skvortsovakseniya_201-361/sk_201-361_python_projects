import random

def level(lvl):
    match lvl:
        case 0:
            summ = 61
            while(summ>60):
                t1 = int(random.randrange(1, 60, 5))
                t2 = int(random.randrange(1, 60, 5))
                summ = t1+t2
            return (t1,t2)
        case 1:
            summ = 181
            while(summ>180):
                t1 = int(random.randrange(1, 180, 5))
                t2 = int(random.randrange(1, 180, 5))
                summ = t1+t2
            return (t1,t2)
        case 2: 
            summ = 481
            while(summ>480):
                t1 = int(random.randrange(1, 480, 5))
                t2 = int(random.randrange(1, 480, 5))
                summ = t1+t2
            return (t1,t2)
        case 3: 
            summ = 1441
            while(summ>1440):
                t1 = int(random.randrange(1, 1440, 5))
                t2 = int(random.randrange(1, 1440, 5))
                summ = t1+t2
            return (t1,t2)
        case _:
            print("Error")

def render(ans, s, p):
    if s == 0:
        if int(ans) == p[0]+p[1]:
            return True
    elif s == 1:
        t = ans.split(":")
        sum = p[0]+p[1]
        if int(t[0]) == int(sum/60) and int(t[1]) == int(sum%60) :
            return True
    else:
        print("Неправильно")
    

answer = "yes"
while(answer == "yes"):
    stat = int(0)
    print("Уровень сложности: 0 - детский, 1 - легкий, 2 - средний, 3 - легкий.")
    lvl = int(input())
    print("В каком формате вы хотите играть? Если в минутах, то введите 0, а если в часах и минутах - 1.\n")
    s = int(input())
    print("Количество вопросов?")
    q = int(input())

    for i in range(q):
        p = level(lvl)
        print(int(p[0]/60), ":", p[0]%60, " + ", int(p[1]/60), ":", p[1]%60, " = ")
        ans = input()
        if render(ans, s, p) == True:
            stat += 1
       
   
    print("Правильно: ", stat, ". Всего вопросов: ", q)
    print("Повторим? Пишите yes")
    answer = input().lower()