# labMatrixRotate
#Создать матрицу и заполнить случайными значениями, распечатать, выбрать в консоли действия над матрицей повернуть по часовой, против, отразить по вертикали, перепечатать результат
# import random
# import numpy as np


matrix = [[random.randint(0,10) for e in range(3)] for e in range(3)]
print(*matrix, sep='\n')

print("0 - отразить по вертикали \n1 - отразить по горизонтали \n2 - повернуть по часовой стрелке на 90 градусов \n3 - повернуть против часовой стрелки на 90 градусов")
v = int(input())

if v == 0:
    m1 = np.fliplr(matrix)#отразить матрицу по вертикали
    print(*m1, sep='\n')
elif v == 1:
    m1 = np.flipud(matrix)#отразить матрицу по горизонтали
    print(*m1, sep='\n')
elif v == 2:
    m2 = np.rot90(matrix,-1)#повернуть матрицу на 90 градусов  по часовой стрелке
    print(*m2, sep='\n')
elif v == 3:
    m2 = np.rot90(matrix)#повернуть матрицу на 90 градусов против часовой стрелки
    print(*m2, sep='\n')
else:
    print("Error")
    # sdlgs