# Нарисовать в консоли прямоугольник с параметрами: ширина, высота, заполнить
# osjdogvs

char = '░'

w = int(input("Ширина?"))
h = int(input("Высота?"))
f = input("Закрасить? [y/n]")
is_fill = f.lower() == 'y'

char_fill = char if is_fill else ' '

for r in range(h):
    if r == 0 or r == h-1:
        print(char * w)
    else:
        print(char, char_fill * (w - 2), char, sep='')