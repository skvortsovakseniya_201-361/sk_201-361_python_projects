# Циклы (for, while)
for i in range(40, 16, -1):
    print(i)

for i in [0, 1, 2]: # список
    print(i)
    

for i in range(3):
    print(i)

print("---")

for i in range(5):
    if i == 2:
        continue
    print(i)
    if i == 3:
        break

for c in "Hello":
    print(c, end=" - ")

print()
print(10, sep=",")
print(*"Hello", sep="-")

i = 0
while i < 3:
    print(i)
    i += 1
    # sdkvs