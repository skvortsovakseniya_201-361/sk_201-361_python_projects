# if 
# if-else
# if-elif-else
# dsngkks

#and, or, not

x = int(input("x = "))
y = int(input("у = "))

# (1)

if x > 0 and y > 0:
    print(1)
else:
    if x < 0 and y > 0:
        print(2)
    else:
        if x < 0 and y < 0:
            print(3)
        else:
            print("другое")

# (2)

if x > 0 and y > 0:
    print(1)
elif x < 0 and y > 0:
    print(2)
elif x < 0 and y < 0:
    print(3)
else:
    print("другое")

#тернальный условный оператор
z = 1
print("Да" if z==1 else "Нет")