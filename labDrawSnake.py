# labDrawSnake
#Нарисовать в консоли прямоугольную змейку с параметрами ширина и высота
# dsjvs
smb = '*'
space = ' '

w = int(input("Width? "))
h = int(input("Height? "))
side = input("Side? [r/l] ") 
side = side.lower()

for i in range(1,h+1):
    if i % 2 == 1:
        print(smb * w)
    else:
        if side == 'r':
            if i % 4 == 2:
                print(smb + space * (w - 1))
            elif i % 4 == 0:
                print(space * (w - 1) + smb)
        else:
            if i % 4 == 2:
                print(space * (w - 1) + smb)
            elif i % 4 == 0:
                 print(smb + space * (w - 1))
