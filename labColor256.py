# labColor256
#Вывести в консоль все доступные цвета и фоны 256
def colors_256(color_):
    n1 = str(color_)
    n2 = str(color_).ljust(3, ' ')
    if color_ % 16 == 0:
        return(f"\033[38;5;{n1}m {n2} \033[0;0m\n")
    else:
        return(f"\033[38;5;{n1}m {n2} \033[0;0m")

def colors_256_bg(color_):
    n1 = str(color_)
    n2 = str(color_).ljust(3, ' ')
    if color_ % 16 == 0:
        return(f"\033[48;5;{n1}m {n2} \033[0;0m\n")
    else:
        return(f"\033[48;5;{n1}m {n2} \033[0;0m")

print("\nВсе доступные цвета:")
print(' '.join([colors_256(x) for x in range(256)]))

print("\nВсе доступные фоны:")
print(' '.join([colors_256_bg(x) for x in range(256)]))
# skdfjgipsdf